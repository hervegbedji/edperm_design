<!DOCTYPE html>
<html class="no-js">
    <head>
        <title> Renseignements etudiant(e)s et superviseur(e)s</title>
        <link rel="stylesheet" type="text/css" href="./css/form_etudiant.css">
        <link rel="stylesheet" type="text/css" href="./css/frame.css">
        <link rel="stylesheet" type="text/css" href="./css/skeleton.css">
        <script type="text/javascript" src="./js/jquery.js"></script>
        <script type="text/javascript" src="./js/html5shiv.js"></script>
        <script type="text/javascript" src="./js/modernizr-latest.js"></script>
        <script type="text/javascript">

        </script>

        <meta charset="utf-8">
    </head>
    <body>
     <div id="wrapper">
          <header class="blue_bar">
            <div class="container">
                <div class="three columns logo">
                    <p class="">
                        <a href="#"> &Eacute;ducation permanente </a>
                    </p>
                </div> <!-- logo -->
                   <!-- +++ -->
                <div class="four columns current_section">
                    <p class="">
                        <a href="#"> path </a>
                    </p>
                </div> <!-- current section -->
                   <!-- +++ -->
                <div class="two columns documentation">
                    <p class=" ">
                        <a href="#"> Documentation </a>
                    </p>
                </div> <!-- documentation -->
                  <!-- +++ -->
                <div class="two columns user_name">
                    <p class="">
                        <a href="#"> John  </a>
                    </p>
                </div> <!-- user menu dropDown -->
              </div> <!-- end container -->
            </header> <!-- blue_bar -->
<!-- = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = -->
             <!-- main content (Ajax loads and all) -->
            <div class="main">
              <div class="container">
                <form class="seven columns studentBooking">
 <!-- *********************************** coordonnées de l'etudiant **************************************** -->
                    <details class="twelve columns studentTab">
                    <summary class="twelve columns"> Coordonn&eacute;es de l'étudiant </summary>
                    <input class="twelve columns" name="" type="text" maxlength="" placeholder="nom"> <br>
                    <input class="twelve columns" name="" type="text" maxlength="" placeholder="prenom"> <br>
                    <input class="twelve columns" name="" type="text" maxlength="" placeholder="NI"> <br>
                    <input class="twelve columns" name="" type="text" maxlength="" placeholder="email"> <br>
                    <input class="twelve columns" name="" type="text" maxlength="" placeholder="adresse"> <br>
                    <input class="twelve columns" name="" type="text" maxlength="" placeholder="telephone jour"> <br>
                    <input class="twelve columns" name="" type="text" maxlength="" placeholder="telephone nuit">
                    </details> <!-- collapsible element Number 1 -->

 <!-- *********************************** choix des exams et des villes **************************************** -->
                    <details class="twelve columns examTab">
                    <summary class="twelve columns"> Concernant l'examen supervisé </summary>
                    <p> Choisir le ou les examens possibles </p>
                    <select class="twelve columns cours_offerts" class="eight columns">
                    <option> Dérouler liste des cours offerts </option>
                    <option value="volvo"> gmec1000</option>
                    <option value="saab"> ardra2340</option>
                    <option value="mercedes">knep3490</option>
                    <option value="audi"> econ3457</option>
                    </select> <br>
                    <p class="twelve columns"> Je désire composer mon examen mi-semestre(intra) au:</p>

                    <!-- inside of nb -->
                     <select id="ville_intra" class="offset-by-one five columns button_InNB">
                     <option> Interieur du NB( dérouler villes)</option>
                     <option value="volvo"> Moncton</option>
                     <option value="saab"> Shippagan</option>
                     <option value="mercedes">Bathurst</option>
                     <option value="audi"> Fredericton</option>
                    </select>

                    <!-- out of nb -->
                    <select id="ville_final" class="five columns button_OutNB">
                     <option> A l'exterieur du N.-B. (dérouler villes) </option>
                     <option value="volvo"> Burkina-Faso</option>
                     <option value="saab"> Taipei</option>
                     <option value="mercedes"> Buenos Aires</option>
                     <option value="audi"> Seoul </option>
                    </select> <br>
                    <p class="twelve columns"> Je désire composer mon examen final au:</p>

                    <!-- inside of nb -->
                     <select id="ville_intra" class="offset-by-one five columns button_InNB">
                     <option> Interieur du NB (dérouler villes)</option>
                     <option value="volvo"> Moncton</option>
                     <option value="saab"> Shippagan</option>
                     <option value="mercedes">Bathurst</option>
                     <option value="audi"> Fredericton</option>
                    </select>

                    <!-- out of nb -->
                    <select id="ville_final" class="five columns button_OutNB">
                     <option> A l'exterieur du N.-B. (derouler villes) </option>
                     <option value="volvo"> Burkina-Faso</option>
                     <option value="saab"> Taipei</option>
                     <option value="mercedes"> Buenos Aires</option>
                     <option value="audi"> Seoul </option>
                    </select>



                      <!-- Choix de ville pour les intra -->


                      <!-- Choix de ville fini -->
                    </details> <!-- end collapsible element number 2 -->

 <!-- *********************************** coordonnées du superviseur **************************************** -->
                    <details class="twelve columns">
                    <summary class="twelve columns"> coordonnées superviseur</summary>
                     <p class="eight columns"> Coordonn&eacute;es du superviseur </p>
                        <div class="twelve columns chooseOption_Sup">
                            <a class="twelve columns" href="#">
                                <p class="twelve columns">Composer avec un ancien superviseur</p>
                            </a>
                            <a class="twelve columns" href="#">
                                <p class="twelve columns">Proposer un nouveau superviseur </p>
                            </a>
                        </div> <!-- choose option sup -->
                        <div class="twelve columns yesNewSupervisor"> <!-- shows up if student wants new sup -->
                     <input class="twelve columns" name="" type="text" maxlength="" placeholder="nom"> <br>
                     <input class="twelve columns" name="" type="text" maxlength="" placeholder="prenom"> <br>
                     <input class="twelve columns" name="" type="text" maxlength="" placeholder="NI"> <br>
                     <input class="twelve columns" name="" type="text" maxlength="" placeholder="email"> <br>
                     <input class="twelve columns" name="" type="text" maxlength="" placeholder="adresse"> <br>
                     <input class="twelve columns" name="" type="text" maxlength="" placeholder="telephone jour">
                     <input class="twelve columns" name="" type="text" maxlength="" placeholder="telephone nuit">
                     <p class="twelve columns"> Communiquer avec le superviseur en ?</p>
                    <label> francais:</label> <input type="checkbox"> <br>
                    <label> anglais:</label> <input type="checkbox">
                        </div> <!-- yes new supervisor -->
                    </details> <!-- end collapsible 3 -->

                    <input class="eight columns" name="" type="submit">
                </form> <!-- global student and supervisor info -->
              </div><!-- container -->
            </div> <!-- end main -->
<!-- = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = -->
            <footer>
               <div class="container">
                <div class="three columns logo">
                        <a href="#">
                            <img>
                       </a>
                </div> <!-- logo -->
                  <!-- +++ -->
                <div class="two columns copyright">
                    <p class="">
                        <a href="#"> copyright 2015 - tous droits reserved  </a>
                    </p>
                </div> <!-- user menu dropDown -->
              </div> <!-- end container -->
            </footer>
      </div> <!-- wrapper -->
    </body>
</html>
