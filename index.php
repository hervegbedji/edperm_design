<!DOCTYPE html>
<html>
    <head>
        <title> welcome </title>
        <meta charset="utf-8">
        <meta>
        <meta>
        <link rel="stylesheet" type="text/css" href="./css/landing_page.css" media="all">
        <link rel="stylesheet" type="text/css" href="./css/skeleton.css" media="all">
    </head>
 <!-- try forms and php queries to see how stuffs work hehehe -->
    <body>
     <div id="wrapper">
       <div class="container">
             <!-- brand slogan starts -->
            <div class="offset-by-three ten columns brand_slogan">
                <p class="twelve columns brand">
                    &Eacute;ducation permanente
                </p> <!-- brand or institution name -->
                <p class="offset-by-three ten columns slogan">
                    L'universit&eacute; en tout temps.
                </p> <!-- slogan -->
            </div> <!-- brand_slogan -->
             <!-- authentication starts -->
            <div class="offset-by-four four columns authentication">
                <form>
                    <input class="twelve columns username" type="text" maxlength="150" placeholder="identifiant">
                    <input class="twelve columns password" type="password" maxlength="150" placeholder="mot de passe">
                    <input class="four columns submit" type="submit" value="entrer">
                </form>
            </div> <!-- authentication -->
             <!-- error starts -->
            <div class="four columns errors">
                <p class="twelve columns"> combinaison incorrecte </p>
                <a class="twelve columns" href=""> mot de passe oubli&eacute; ?</a>
            </div> <!-- errors -->
             <!-- footer starts -->
            <div class="twelve columns footer">
                <a class="one column" href="http://www.umoncton.ca"><img src="./images/logo_udem.png"></a>
                <p class="five columns"> &copy; 2015, Universit&eacute; de Moncton.
                 Tous droits r&eacute;serv&eacute;s</p>
            </div> <!-- footer -->
         </div> <!-- container -->
     </div> <!-- end wrapper -->

    </body>
</html>
