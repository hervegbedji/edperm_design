<!DOCTYPE html>
<html>
    <head>
        <title> Renseignements etudiant(e)s et superviseur(e)s</title>
        <link rel="stylesheet" type="text/css" href="./css/form_admin.css">
        <link rel="stylesheet" type="text/css" href="./css/frame.css">
        <link rel="stylesheet" type="text/css" href="./css/skeleton.css">
        <script type="text/javascript" src="./js/jquery.js"></script>
        <meta charset="utf-8">
    </head>
    <body>
     <div id="wrapper">
          <header class="blue_bar">
            <div class="container">
                <div class="three columns logo">
                    <p class="">
                        <a href="#"> &Eacute;ducation permanente </a>
                    </p>
                </div> <!-- logo -->
                   <!-- +++ -->
                <div class="four columns current_section">
                    <p class="">
                        <a href="#"> path </a>
                    </p>
                </div> <!-- current section -->
                   <!-- +++ -->
                <div class="two columns documentation">
                    <p class=" ">
                        <a href="#"> Documentation </a>
                    </p>
                </div> <!-- documentation -->
                  <!-- +++ -->
                <div class="two columns user_name">
                    <p class="">
                        <a href="#"> John  </a>
                    </p>
                </div> <!-- user menu dropDown -->
              </div> <!-- end container -->
            </header> <!-- blue_bar -->
<!-- = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = -->
             <!-- main content (Ajax loads and all) -->
            <div class="main">
              <div class="container">
               <form method="" action="" class="eight columns mount_session">
                  <div class="eight columns first_step">
                    <p class="twelve columns"> Informations sur la session </p>
                     <input class="twelve columns" name="" type="text" maxlength="" placeholder="nom de session"> <br>
                    <input class="twelve columns" name="" type="text" maxlength="" placeholder="ann&eacute;e"> <br>
                    <button> infos cours > </button>
                  </div> <!-- end first setp -->
                  <div class="twelve columns second_step">
                    <p class="twelve columns"> Informations sur le cours </p>
                     <p> choisir le fichier cours</p>
                     <input class="six columns" name="" type="file" maxlength="" placeholder="importer fichier"> <br>
                     <p> choisir le fichier etudiant </p>
                     <input class="six columns" name="" type="file" maxlength="" placeholder="importer fichier"> <br>
                     <input class="six columns" name="" type="text" maxlength="" placeholder="sigle du cours">
                     <div class="six columns courseInstant">

                     </div>
                    <input class="six columns" name="" type="text" maxlength="" placeholder="professeur"> <br>
                    <input class="six columns" name="" type="text" maxlength="" placeholder="date intra"> <br>
                    <input class="six columns" name="" type="text" maxlength="" placeholder="date final"> <br>
                    <button> ajouter un cours </button>
                  </div> <!-- end second setp -->
                  <input type="submit" value="Submit">
               </form>
              </div><!-- container -->
            </div> <!-- end main -->
<!-- = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = -->
            <footer>
               <div class="container">
                <div class="three columns logo">
                        <a href="#">
                            <img>
                       </a>
                </div> <!-- logo -->
                  <!-- +++ -->
                <div class="two columns copyright">
                    <p class="">
                        <a href="#"> copyright 2015 - tous droits reserved  </a>
                    </p>
                </div> <!-- user menu dropDown -->
              </div> <!-- end container -->
            </footer>
      </div> <!-- wrapper -->
    </body>
</html>
